
class Player:
    """
    This class represents player behaviour.
    """

    def __init__(self, name):
        self.name = name
        self.weapon = None
        self.key = None
        self.location = None

    def enter_room(self, room):
        """
        This method moves player to specified room.
        :param room: room id where to enter
        """
        self.location = room

    def search_secret_container(self):
        """
        This method search secret container in current room
        if player location is not hall.
        Set key or weapon flag to True if founded
        """
        if self.location:
            founded = self.location.search_secret_container()
            if founded:
                if founded == 'key':
                    print('You have unlock a secret case. '
                          'Item inside - key')
                    self.key = True
                if founded == 'weapon':
                    print('You have unlock a secret case. '
                          'Item inside - weapon')
                    self.weapon = True
            else:
                print('No secrets in this room')

    def use_exit_portal(self):
        """
        This method search exit portal in current room.
        :return: True if exit was successful
        """
        if self.location:
            return self.location.use_exit_portal(
                self.key, self.weapon)

    def leave_room(self):
        """
        This method moves player from current room.
        """
        if self.location:
            self.location = None
