
class SecretContainer3DigitDigitalLock:
    """
    This class represents secret container behaviour.
    """
    def __init__(self, hidden_item, password):
        self.hidden_item = hidden_item
        self.password = password

    def open(self):
        """
        This method implements container opening
        logic.
        :return: hidden item if container was unlocked
        """
        print('You need to guess combination of 3 digit.'
              'Try to enter sequence of 3 digit.\n'
              'To exit print exit\n')

        input_combination = input('Put combination of 3 digit or exit: ')
        while input_combination != self.password:
            input_combination = input('Put combination of 3 digit or exit: ')
            if input_combination == 'exit':
                return False

        return self.hidden_item
