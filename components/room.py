import random


class Room:
    """
    This class represents room behaviour.
    """
    def __init__(self,
                 secret_container=None,
                 exit_portal=None):
        self.secret_container = secret_container
        self.exit_portal = exit_portal

    def search_secret_container(self):
        """
        This method tries to open container if such
        exists in current room.
        :return: item from secret container
        """
        if self.secret_container:
            secret_item = self.secret_container.open()
            if secret_item:
                self.secret_container = None
            return secret_item
        else:
            return None

    def use_exit_portal(self, key, weapon):
        """
        This method tries to use exit portal
        if such exists and user has all needed items
        (key and  weapon)
        :param key: user key
        :param weapon: user weapon
        :return: True if user can exit
        """
        if self.exit_portal:
            if key:
                if weapon:
                    return self._fight_with_guard()
                else:
                    print('You need weapon to fight with guardian')
            else:
                print('You need key to open exit portal')
        else:
            print('There is no exit portal in this room')

        return False

    def place_secret_container(self, secret_container_type, secret_item):
        """
        This method place secret container in
        current room.
        :param secret_container_type: type that implements
                                      secret_container interface
        :param secret_item: secret item to place into container
        """
        digit = random.randint(0, 9)
        password = ''.join(str(digit) for _ in range(3))
        self.secret_container = secret_container_type(secret_item, password)

    def _fight_with_guard(self):
        """
        Here can be placed fighting logic
        """
        print('Victory')
        return True
