import random

from components.player import Player
from components.room import Room
from components.secret_container import SecretContainer3DigitDigitalLock


class GameFactory:
    """
    This is the basic class of game factory.
    All factories should extend this class and
    implement it's methods.
    """

    def create_player(self, player_name):
        """
        This method create a player object
        Should be implemented in child classes
        """
        pass

    def create_level(self):
        """
        This method create a level object
        Should be implemented in child classes
        """
        pass


class FourRoomsGameFactory(GameFactory):
    """
    This is a factory implementation
    for simple game.
    """
    def create_player(self, player_name):
        """
        This method creates a player object.
        :param player_name: name of player
        :return: Player object
        """
        return Player(player_name)

    def create_level(self):
        """
        This method creates a level objects.
        :return: A list of Room objects.
        """
        rooms = [Room() for _ in range(4)]
        rooms[0].exit_portal = True
        rooms[1].place_secret_container(
            SecretContainer3DigitDigitalLock, 'weapon')
        rooms[2].place_secret_container(
            SecretContainer3DigitDigitalLock, 'key')
        random.shuffle(rooms)
        return rooms
