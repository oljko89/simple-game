from unittest import TestCase
from unittest.mock import patch

from components.player import Player
from components.room import Room


class TestPlayer(TestCase):

    def test_enter_room(self):
        player_obj = Player('Super_player')
        player_obj.enter_room('room_1')
        self.assertEqual(player_obj.location, 'room_1')

    def test_search_secret_container(self):
        player_obj = Player('Super_player')
        player_obj.search_secret_container()
        self.assertFalse(player_obj.key)
        self.assertFalse(player_obj.weapon)

    @patch('components.room.Room.search_secret_container')
    def test_search_secret_container_1(self, mock_search):
        mock_search.return_value = ''
        player_obj = Player('Super_player')
        player_obj.location = Room()
        player_obj.search_secret_container()
        self.assertFalse(player_obj.key)
        self.assertFalse(player_obj.weapon)

    @patch('components.room.Room.search_secret_container')
    def test_search_secret_container_2(self, mock_search):
        mock_search.return_value = 'key'
        player_obj = Player('Super_player')
        player_obj.location = Room()
        player_obj.search_secret_container()
        self.assertTrue(player_obj.key)
        self.assertFalse(player_obj.weapon)

    @patch('components.room.Room.search_secret_container')
    def test_search_secret_container_3(self, mock_search):
        mock_search.return_value = 'weapon'
        player_obj = Player('Super_player')
        player_obj.location = Room()
        player_obj.search_secret_container()
        self.assertFalse(player_obj.key)
        self.assertTrue(player_obj.weapon)

    def test_use_exit_portal(self):
        player_obj = Player('Super_player')
        result = player_obj.use_exit_portal()
        self.assertFalse(result)

    @patch('components.room.Room.use_exit_portal')
    def test_use_exit_portal_1(self, mock_use_exit):
        mock_use_exit.return_value = True
        player_obj = Player('Super_player')
        player_obj.location = Room()
        result = player_obj.use_exit_portal()
        self.assertTrue(result)

    @patch('components.room.Room.use_exit_portal')
    def test_use_exit_portal_2(self, mock_use_exit):
        mock_use_exit.return_value = False
        player_obj = Player('Super_player')
        player_obj.location = Room()
        result = player_obj.use_exit_portal()
        self.assertFalse(result)

    def test_leave_room(self):
        player_obj = Player('Super_player')
        player_obj.location = Room()
        player_obj.leave_room()
        self.assertFalse(player_obj.location)
