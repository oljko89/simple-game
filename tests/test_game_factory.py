from unittest import TestCase
from unittest.mock import patch

from components.game_factory import FourRoomsGameFactory
from components.player import Player


class TestGameFactory(TestCase):

    def test_create_player(self):
        factory = FourRoomsGameFactory()
        player = factory.create_player('pl1')
        self.assertEqual(player.__class__, Player)
        self.assertEqual(player.name, 'pl1')

    @patch('random.shuffle')
    def test_create_level(self, mock_shuffle):
        factory = FourRoomsGameFactory()
        rooms = factory.create_level()
        self.assertEqual(len(rooms), 4)
        self.assertTrue(rooms[0].exit_portal)
        self.assertTrue(rooms[1].secret_container)
        self.assertFalse(rooms[1].exit_portal)
        self.assertTrue(rooms[2].secret_container)
