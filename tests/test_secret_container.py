from unittest import TestCase
from unittest.mock import patch

from components.secret_container import SecretContainer3DigitDigitalLock


class TestSecretContainer(TestCase):

    @patch('builtins.input')
    def test_open(self, mock_input):
        mock_input.side_effect = ['222', '333',
                                  '555', '444']
        secret_obj =\
            SecretContainer3DigitDigitalLock('key',
                                             '555')
        secret_obj.open()
        self.assertEqual(secret_obj.hidden_item, 'key')
        self.assertEqual(mock_input.call_count, 3)
