from unittest import TestCase
from unittest.mock import patch

from components.room import Room
from components.secret_container import SecretContainer3DigitDigitalLock


class TestRoom(TestCase):

    def test_search_secret_container(self):
        room_obj = Room()
        result = room_obj.search_secret_container()
        self.assertEqual(result, None)

    @patch('components.secret_container.'
           'SecretContainer3DigitDigitalLock.open')
    def test_search_secret_container_1(self, mock_open):
        hidden_item = 'key'
        mock_open.return_value = hidden_item
        container = SecretContainer3DigitDigitalLock(hidden_item,
                                                     '000')
        room_obj = Room(secret_container=container)
        result = room_obj.search_secret_container()
        self.assertEqual(result, hidden_item)
        self.assertFalse(room_obj.secret_container)

    @patch('components.secret_container.'
           'SecretContainer3DigitDigitalLock.open')
    def test_search_secret_container_2(self, mock_open):
        mock_open.return_value = False
        container = SecretContainer3DigitDigitalLock('key',
                                                     '000')
        room_obj = Room(secret_container=container)
        result = room_obj.search_secret_container()
        self.assertEqual(result, False)
        self.assertEqual(room_obj.secret_container, container)

    @patch('components.room.Room._fight_with_guard')
    def test_use_exit_portal(self, mock_fight):
        mock_fight.return_value = True
        room_obj = Room(exit_portal=True)
        result = room_obj.use_exit_portal(True, True)
        self.assertTrue(result)

    def test_use_exit_portal_1(self):
        room_obj = Room(exit_portal=False)
        result = room_obj.use_exit_portal(True, True)
        self.assertFalse(result)

    def test_use_exit_portal_2(self):
        room_obj = Room(exit_portal=True)
        result = room_obj.use_exit_portal(True, False)
        self.assertFalse(result)

    def test_use_exit_portal_3(self):
        room_obj = Room(exit_portal=True)
        result = room_obj.use_exit_portal(False, True)
        self.assertFalse(result)

    @patch('random.randint')
    def test_place_secret_container(self, mock_random):
        mock_random.return_value = 5
        room_obj = Room()
        room_obj.place_secret_container(
            SecretContainer3DigitDigitalLock,
            'key')
        self.assertEqual(room_obj.secret_container.password,
                         '555')
