import datetime

from components.game_factory import FourRoomsGameFactory
from game_config import MENU_ITEMS


class Game:
    """
    This is core game class where all things are connected.
    """
    def __init__(self, game_factory):
        self.game_factory = game_factory
        self.rooms = None
        self.player = None
        self.player_wins = False
        self.game_finished = False
        self.menu = None
        self.game_started = datetime.datetime.now()

    def play(self):
        """
        This method prints menu and handle user choice
        while game is not finished.
        """
        self.init_game()
        while not self.game_finished:
            self.print_menu()
            self.handle_user_action('Chosen action:')

    def print_menu(self):
        """
        This method prints user menu
        :return:
        """
        print(MENU_ITEMS['header'])
        print('Items in your bag: {} {}'.
              format('key,' if self.player.key else '',
                     'weapon,' if self.player.weapon else ''))
        if self.menu:
            print(self.menu[0])

    def handle_user_action(self, message):
        """
        This method handles user commands and executes
        action if command is valid.
        :param message:
        """
        user_choice = input(message)
        available_actions = self.menu[1]
        if user_choice not in available_actions:
            print('Wrong choice. Press Enter')
            input()
        else:
            if user_choice == 'exit':
                self.finish_game(forced=True)
            elif user_choice == 'er1':
                self.player.enter_room(self.rooms[0])
                self.menu = MENU_ITEMS['room']
            elif user_choice == 'er2':
                self.player.enter_room(self.rooms[1])
                self.menu = MENU_ITEMS['room']
            elif user_choice == 'er3':
                self.player.enter_room(self.rooms[2])
                self.menu = MENU_ITEMS['room']
            elif user_choice == 'er4':
                self.player.enter_room(self.rooms[3])
                self.menu = MENU_ITEMS['room']
            elif user_choice == 'lr':
                self.player.leave_room()
                self.menu = MENU_ITEMS['main']
            elif user_choice == 'ssc':
                self.player.search_secret_container()
            elif user_choice == 'uep':
                if self.player.use_exit_portal():
                    self.finish_game()
            input()

    def init_game(self):
        """
        This method initialized all needed parts
        before game starts.
        """
        player_name = input('Please enter player name: ')
        self.menu = MENU_ITEMS['main']
        self.player = self.game_factory.create_player(player_name)
        self.rooms = self.game_factory.create_level()

    def finish_game(self, forced=False):
        """
        This method ends the game and prints statistic
        :param forced: True if user exit game by command
        """
        if forced:
            print('Exited by User')
        else:
            self.print_statistics()
        self.game_finished = True

    def print_statistics(self):
        """
        This method print statistics.
        """
        game_duration = datetime.datetime.now() - self.game_started
        print('Congratulations {}, '
              'you have finished this level in - {}'
              .format(self.player.name, game_duration))


def main():
    """
    Entry point for game execution
    """
    game = Game(FourRoomsGameFactory())
    game.play()


if __name__ == '__main__':
    main()
